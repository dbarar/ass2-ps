package sample;

import constants.Consts;
import repository.BookRepository;
import repository.UserRepository;
import repository.impl.BookRepositoryImpl;
import repository.impl.JDBConnectionWrapper;
import repository.impl.UserRepositoryImpl;
import service.AdministratorService;
import service.ContextHolder;
import service.EmployeeService;
import service.UserService;
import service.impl.AdministratorServiceImpl;
import service.impl.ContextHolderImpl;
import service.impl.EmployeeServiceImpl;
import service.impl.UserServiceImpl;

import java.util.logging.Logger;

public class ClassInstances {
    //logger
    private static Logger logger = Consts.logger;
    private static ClassInstances instance = null;

    //init db connection
    private static JDBConnectionWrapper jdbConnectionWrapper = new JDBConnectionWrapper(Consts.SCHEMA_NAME);

    //init repositories
    private static UserRepository userRepository = new UserRepositoryImpl(jdbConnectionWrapper, logger);
    private static BookRepository bookRepository = new BookRepositoryImpl(jdbConnectionWrapper, logger);

    //init services
    private static ContextHolder contextHolder = new ContextHolderImpl();
    private static UserService userService = new UserServiceImpl(logger);
    private static AdministratorService adminUserService = new AdministratorServiceImpl(logger);
    private static EmployeeService employeeService = new EmployeeServiceImpl(logger);


    public static UserRepository getUserRepository() {
        return userRepository;
    }

    public static BookRepository getBookRepository() {
        return bookRepository;
    }

    public static AdministratorService getAdminUserService() {
        return adminUserService;
    }

    public static EmployeeService getEmployeeService() {
        return employeeService;
    }

    public static JDBConnectionWrapper getJdbConnectionWrapper() {
        return jdbConnectionWrapper;
    }

    public static ContextHolder getContextHolder() {
        return contextHolder;
    }

    public static UserService getUserService() {
        return userService;
    }


    private ClassInstances() {

    }

    public static ClassInstances getInstance(){
        if (instance == null){
            instance = new ClassInstances();
        }
        return instance;
    }
}
