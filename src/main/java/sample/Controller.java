package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.User;
import model.enumeration.UserRole;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    @FXML
    ComboBox<String> userTypeId = new ComboBox<>();
    @FXML
    Button startButton;
    @FXML
    TextField usernameId;
    @FXML
    TextField passwordId;

    public void startButtonEvent(ActionEvent event){
        String username = usernameId.getText();
        String password = passwordId.getText();
        User user = ClassInstances.getUserService().login(username, password);
        if (user == null){
            new Alert(Alert.AlertType.ERROR, "This is an error!").showAndWait();
        }else{
            if (user.getUserRole().name().equals(userTypeId.getSelectionModel().getSelectedItem())){
                if(user.getUserRole() == UserRole.USER){
                    try {
                        Parent root = FXMLLoader.load(getClass().getResource("/EmployeeView.fxml"));
                        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                        app_stage.setTitle("Book store - Employee");
                        app_stage.setScene(new Scene(root));
                        app_stage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    try {
                        Parent root = FXMLLoader.load(getClass().getResource("/AdminView.fxml"));
                        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                        app_stage.setTitle("Book store - Administrator");
                        app_stage.setScene(new Scene(root));
                        app_stage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        userTypeId.getItems().addAll("USER", "ADMIN");
    }
}
