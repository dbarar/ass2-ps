package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Notification {
    private List<String> errors = new ArrayList<>();

    public String errorMessage() {
        return errors.stream()
                .collect(Collectors.joining(", "));
    }

    public void addError(String message) {
        errors.add(message);
    }

    public boolean hasErrors() {
        return ! errors.isEmpty();
    }

    public List getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}