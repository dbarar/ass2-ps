package repository;

import model.User;
import model.enumeration.UserRole;

import java.util.List;

public interface UserRepository {
    User create(UserRole role, String username, String password);

    User findById(Long id);

    List<User> findAll();

    User update(User user);

    boolean delete(Long id);

    public User loadByUserName(String userName);
}
