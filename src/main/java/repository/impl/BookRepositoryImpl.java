package repository.impl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Book;
import model.builder.BookBuilder;
import repository.BookRepository;
import sample.ClassInstances;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class BookRepositoryImpl implements BookRepository {
    private final JDBConnectionWrapper jdbConnectionWrapper;

    private final Logger logger;
    private Connection connection;
    private String sqlQuery;
    private ResultSet resultSet;
    private PreparedStatement preparedStatement;
    private Book book;
    private List<Book> books;

    public BookRepositoryImpl(JDBConnectionWrapper jdbConnectionWrapper, Logger logger) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
        this.logger = logger;
    }


    @Override
    public Book create(String genre, String title, String author, Long quantity, Double price) {
        sqlQuery = "INSERT INTO book (genre, title, author, quantity, price) " +
                "VALUES (?, ?, ?, ?, ?)";
        book = new BookBuilder()
                .withGenre(genre)
                .withTitle(title)
                .withAuthor(author)
                .withQuantity(quantity)
                .withPrice(price)
                .build();
        connection = jdbConnectionWrapper.getConnection();
        try{
            preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, genre);
            preparedStatement.setString(2, title);
            preparedStatement.setString(3, author);
            preparedStatement.setLong(4, quantity);
            preparedStatement.setDouble(5, price);

            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next())
                book.setId(resultSet.getLong(1));

            resultSet.close();
        }catch (SQLException e){
            e.printStackTrace();
            logger.info("While creating the book, the following exception appeared" + e);
            return null;
        }
        logger.info("Created book:" + book);

        return book;
    }

    @Override
    public Book findById(Long id) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM book WHERE id=?";

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                book = new BookBuilder()
                        .withId(resultSet.getLong(1))
                        .withGenre(resultSet.getString(2))
                        .withTitle(resultSet.getString(3))
                        .withAuthor(resultSet.getString(4))
                        .withQuantity(resultSet.getLong(5))
                        .withPrice(resultSet.getDouble(6))
                        .build();
                logger.info("Book found by id: " + book);
                return book;
            }

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While finding the book by id, the following exception appeared " + e);
        }

        return null;
    }

    @Override
    public List<Book> findByGenre(String genre) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM book WHERE genre = ?";

        books = new ArrayList<>();

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, genre);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                book = new BookBuilder()
                        .withId(resultSet.getLong(1))
                        .withGenre(resultSet.getString(2))
                        .withTitle(resultSet.getString(3))
                        .withAuthor(resultSet.getString(4))
                        .withQuantity(resultSet.getLong(5))
                        .withPrice(resultSet.getDouble(6))
                        .build();

                logger.info("Book found by genre: " + book);
                books.add(book);
            }
            resultSet.close();

            return books;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Exception while finding all books by genre" + e);
        }
        return null;
    }

    @Override
    public List<Book> findByAuthor(String author) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM book WHERE author = ?";

        books = new ArrayList<>();

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, author);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                book = new BookBuilder()
                        .withId(resultSet.getLong(1))
                        .withGenre(resultSet.getString(2))
                        .withTitle(resultSet.getString(3))
                        .withAuthor(resultSet.getString(4))
                        .withQuantity(resultSet.getLong(5))
                        .withPrice(resultSet.getDouble(6))
                        .build();

                logger.info("Book found by author: " + book);
                books.add(book);
            }
            resultSet.close();

            return books;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Exception while finding all books by author" + e);
        }
        return null;
    }

    @Override
    public List<Book> findByTitle(String title) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM book WHERE title=?";

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, title);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                book = new BookBuilder()
                        .withId(resultSet.getLong(1))
                        .withGenre(resultSet.getString(2))
                        .withTitle(resultSet.getString(3))
                        .withAuthor(resultSet.getString(4))
                        .withQuantity(resultSet.getLong(5))
                        .withPrice(resultSet.getDouble(6))
                        .build();

                logger.info("Book found by title: " + book);
                books.add(book);
            }
            resultSet.close();

            return books;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While finding the book by title, the following exception appeared " + e);
        }

        return null;
    }

    @Override
    public List<Book> findAll() {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM book";
        books = new ArrayList<>();

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                book = new BookBuilder()
                        .withId(resultSet.getLong(1))
                        .withGenre(resultSet.getString(2))
                        .withTitle(resultSet.getString(3))
                        .withAuthor(resultSet.getString(4))
                        .withQuantity(resultSet.getLong(5))
                        .withPrice(resultSet.getDouble(6))
                        .build();

                logger.info("Book found by id: " + book);
                books.add(book);
            }
            resultSet.close();

            return books;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Exception while finding all books" + e);
        }
        return null;
    }

    @Override
    public Book update(Book book) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "UPDATE book SET genre=?, title=?, author=?, quantity=?, price=? WHERE id=?;";
        int changedRows;

        try {
            preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, book.getGenre());
            preparedStatement.setString(2, book.getTitle());
            preparedStatement.setString(3, book.getAuthor());
            preparedStatement.setLong(4, book.getQuantity());
            preparedStatement.setDouble(5, book.getPrice());
            preparedStatement.setLong(6, book.getId());

            changedRows = preparedStatement.executeUpdate();
            if (changedRows > 0){
                logger.info("Just updated the book: " + book);
                return book;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While updating book, the following exception appeared" + e);
        }
        return null;
    }

    @Override
    public boolean delete(Long id) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "DELETE FROM book  WHERE id = ?;";

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, id);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Exception while deleting book with id: " + id + e);
            return false;
        }

        logger.info("Deleted the book with the id: " + id);
        return true;
    }
}
