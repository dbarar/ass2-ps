package repository;

import javafx.collections.ObservableList;
import model.Book;

import java.util.List;

public interface BookRepository {
    Book create(String genre, String title, String author, Long quantity, Double price);

    Book findById(Long id);

    List<Book> findByGenre(String genre);

    List<Book> findByAuthor(String author);

    List<Book> findByTitle(String title);

    List<Book> findAll();

    Book update(Book book);

    boolean delete(Long id);
}
