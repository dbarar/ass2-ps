package controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Book;
import model.User;
import model.enumeration.UserRole;
import sample.ClassInstances;

import java.net.URL;
import java.util.ResourceBundle;


public class AdminController implements Initializable {

    @FXML
    TableView<Book> tableId;
    @FXML
    TableColumn<Book, Long> itemIdCol;
    @FXML
    TableColumn<Book, String> itemGenreCol;
    @FXML
    TableColumn<Book, String> itemTitleCol;
    @FXML
    TableColumn<Book, String> itemAuthorCol;
    @FXML
    TableColumn<Book, Long> itemQuantityCol;
    @FXML
    TableColumn<Book, Double> itemPriceCol;

    //for users
    @FXML
    TableView<User> tableUsersId;
    @FXML
    TableColumn<User, Long> itemIdUserCol;
    @FXML
    TableColumn<User, UserRole> itemRoleUserCol;
    @FXML
    TableColumn<User, String> itemUsernameUserCol;
    @FXML
    TableColumn<User, String> itemPasswordUserCol;

    @FXML
    TextField idBookTextField;
    @FXML
    TextField genreTextField;
    @FXML
    TextField titleTextField;
    @FXML
    TextField authorTextField;
    @FXML
    TextField quantityTextField;
    @FXML
    TextField priceTextField;
    @FXML
    TextField idUserTextField;
    @FXML
    TextField roleTextField;
    @FXML
    TextField usernameTextField;
    @FXML
    TextField passwordTextField;

    @FXML
    Button addBookButton;
    @FXML
    Button deleteBookButton;
    @FXML
    Button updateBookButton;
    @FXML
    Button getBookButton;
    @FXML
    Button addUserButton;
    @FXML
    Button deleteUserButton;
    @FXML
    Button updateUserButton;
    @FXML
    Button getUserButton;

    @FXML
    Button cvsButton;
    @FXML
    Button pdfButton;

    private ObservableList<Book> books = ClassInstances.getAdminUserService().getBooksObservable();
    private ObservableList<User> users = ClassInstances.getAdminUserService().getUsersObservable();

    @Override
    public void initialize(URL location, ResourceBundle resources){
        buildBookTableViewData();
        buildUserTableViewData();
    }

    private void buildBookTableViewData(){
        itemIdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        itemGenreCol.setCellValueFactory(new PropertyValueFactory<>("genre"));
        itemTitleCol.setCellValueFactory(new PropertyValueFactory<>("title"));
        itemAuthorCol.setCellValueFactory(new PropertyValueFactory<>("author"));
        itemQuantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        itemPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));

        tableId.setItems(books);

    }

    private void buildUserTableViewData(){
        itemIdUserCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        itemRoleUserCol.setCellValueFactory(new PropertyValueFactory<>("userRole"));
        itemUsernameUserCol.setCellValueFactory(new PropertyValueFactory<>("username"));
        itemPasswordUserCol.setCellValueFactory(new PropertyValueFactory<>("password"));

        tableUsersId.getItems().clear();
        tableUsersId.getItems().addAll(users);
        tableUsersId.setItems(users);
    }

    public void addBook(ActionEvent event){
        ClassInstances.getAdminUserService().createBook(genreTextField.getText(),
                titleTextField.getText(),
                authorTextField.getText(),
                Long.parseLong(quantityTextField.getText()),
                Double.parseDouble(priceTextField.getText())
                );
    }

    public void updateBook(){
        ClassInstances.getAdminUserService().updateBook(Long.parseLong(idBookTextField.getText()),
                genreTextField.getText(),
                titleTextField.getText(),
                authorTextField.getText(),
                Long.parseLong(quantityTextField.getText()),
                Double.parseDouble(priceTextField.getText())
        );
    }

    public void deleteBook(ActionEvent event){
        ClassInstances.getAdminUserService().deleteBook(Long.parseLong(idBookTextField.getText()));
    }

    public void getBook(){
        Book book = ClassInstances.getAdminUserService().findBookById(Long.parseLong(idBookTextField.getText()));
        genreTextField.setText(book.getGenre());
        titleTextField.setText(book.getTitle());
        authorTextField.setText(book.getAuthor());
        quantityTextField.setText(book.getQuantity().toString());
        priceTextField.setText(book.getPrice().toString());
    }

    public void addUser(ActionEvent event){
        ClassInstances.getAdminUserService().createEmployee(UserRole.valueOf(roleTextField.getText()),
                usernameTextField.getText(),
                passwordTextField.getText()
        );
    }

    public void updateUser(){
        ClassInstances.getAdminUserService().updateEmployee(Long.parseLong(idUserTextField.getText()),
                UserRole.valueOf(roleTextField.getText()),
                usernameTextField.getText(),
                passwordTextField.getText()
        );
    }

    public void deleteUser(ActionEvent event){
        ClassInstances.getAdminUserService().deleteEmployee(Long.parseLong(idUserTextField.getText()));
    }

    public void getUser(){
        User user = ClassInstances.getAdminUserService().findEmployeeById(Long.parseLong(idUserTextField.getText()));
        roleTextField.setText(user.getUserRole().name());
        usernameTextField.setText(user.getUsername());
        passwordTextField.setText(user.getPassword());
    }

    public void writePDF(){
        ClassInstances.getAdminUserService().writeReport("PDF");
    }

    public void writeCVS() {
        ClassInstances.getAdminUserService().writeReport("CSV");
    }
}
