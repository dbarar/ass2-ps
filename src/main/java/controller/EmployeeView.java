package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import model.Book;
import sample.ClassInstances;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class EmployeeView implements Initializable {

    @FXML
    TextField genreTextField;
    @FXML
    TextField authorTextField;
    @FXML
    TextField titleTextField;
    @FXML
    TextField idTextField;
    @FXML
    TextField quantityTextField;
    @FXML
    TextArea bookTextArea;
    @FXML
    Button sellButton;
    @FXML
    Button searchButton;

    public void searchBooks(ActionEvent event) {
        String Genre = genreTextField.getText();
        String Author = authorTextField.getText();
        String Title = titleTextField.getText();

        java.util.List<Book> books;
        if (!Genre.isEmpty() && Author.isEmpty() && Title.isEmpty()) {
            bookTextArea.setText(ClassInstances.getEmployeeService().searchBooksByGenre(Genre).toString());
        }
        if (Genre.isEmpty() && !Author.isEmpty() && Title.isEmpty()) {
            bookTextArea.setText(ClassInstances.getEmployeeService().searchBookByAuthor(Author).toString());
        }
        if (Genre.isEmpty() && Author.isEmpty() && !Title.isEmpty()) {
            bookTextArea.setText(ClassInstances.getEmployeeService().searchBooksByTitle(Title).toString());
        }
        if (Genre.isEmpty() && !Author.isEmpty() && !Title.isEmpty()) {
            bookTextArea.setText(filter2Items("Author", Author, Title).toString());
        }
        if (!Genre.isEmpty() && Author.isEmpty() && !Title.isEmpty()) {
            bookTextArea.setText(filter2Items("Genre", Genre, Title).toString());
        }
        if (!Genre.isEmpty() && !Author.isEmpty() && Title.isEmpty()) {
            bookTextArea.setText(filter2Items("Genre", Genre, Author).toString());
        }
        if (!Genre.isEmpty() && !Author.isEmpty() && !Title.isEmpty()) {
            bookTextArea.setText(filter3Items("Genre", Genre, Author, Title).toString());
        }
    }

    private java.util.List<Book> filter2Items(String chrMethod, String item1, String item2) {
        Method method = null;
        String nameMethod = "searchBooksBy" + chrMethod;
        try {
            method = ((Object) ClassInstances.getEmployeeService()).getClass().getMethod(nameMethod, String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        if (!item1.isEmpty() && !item2.isEmpty()) {
            try {
                java.util.List<Book> books = (List<Book>) method.invoke(((Object) ClassInstances.getEmployeeService()), item1);
                books = books.stream()
                        .filter(book -> book.getAuthor().equals(item2))
                        .collect(Collectors.toList());

//                books.stream()
//                        .filter(one -> {
//                            if (item1 != null && item1.length()>0) {
//                                return one.getTitle().equals(item1);
//                            }
//                            return true;
//                        })
//                        .f
                return books;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private java.util.List<Book> filter3Items(String chrMethod, String item1, String item2, String item3) {
        Method method = null;
        String nameMethod = "searchBooksBy" + chrMethod;
        try {
            method = ((Object) ClassInstances.getEmployeeService()).getClass().getMethod(nameMethod, String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        if (!item1.isEmpty() && !item2.isEmpty()) {
            try {
                java.util.List<Book> books = (List<Book>) method.invoke(((Object) ClassInstances.getEmployeeService()), item1);
                books = books.stream()
                        .filter(book -> book.getAuthor().equals(item2) && book.getTitle().equals(item3))
                        .collect(Collectors.toList());
                return books;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public void sellBook(ActionEvent event) {
        Long idToSell = Long.parseLong(idTextField.getText());
        try {
            Book book = ClassInstances.getEmployeeService().sellBook(idToSell);
            bookTextArea.setText(book.toString());
        } catch (Exception e) {
            bookTextArea.setText(e.toString());
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}


