package service.impl;

import model.User;
import model.builder.UserBuilder;
import model.enumeration.UserRole;
import repository.BookRepository;
import repository.UserRepository;
import sample.ClassInstances;
import service.ContextHolder;
import service.UserService;

import java.util.logging.Logger;

public class UserServiceImpl implements UserService {

    protected final UserRepository userRepository = ClassInstances.getUserRepository();
    protected final BookRepository bookRepository = ClassInstances.getBookRepository();
    private final ContextHolder contextHolder = ClassInstances.getContextHolder();
    protected final Logger logger;

    public UserServiceImpl(Logger logger) {
        this.logger = logger;
    }

    @Override
    public User login(String userName, String password) {
        User user = userRepository.loadByUserName(userName);
        if(user != null) {
            if(user.getPassword().equals(password)) {
                contextHolder.setLoggedInUser(user);
                logger.info("User just got logged in: " + user);
                return user;
            } else {
                logger.warning("Wrong password for user " + userName);
            }
        } else {
            logger.warning("User with username: " + userName + " was not found");
        }
        return null;
    }

    @Override
    public void logout() {
        contextHolder.setLoggedInUser(null);
        logger.info("User logged out");
    }
}
