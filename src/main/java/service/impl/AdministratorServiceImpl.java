package service.impl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Book;
import model.User;
import model.builder.BookBuilder;
import model.builder.UserBuilder;
import model.enumeration.UserRole;
import sample.ClassInstances;
import service.AdministratorService;
import service.reportFactory.CSVReport;
import service.reportFactory.PDFReport;
import service.reportFactory.ReportFactory;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class AdministratorServiceImpl extends UserServiceImpl implements AdministratorService {
    private ObservableList<Book> booksObservable = FXCollections.observableArrayList();
    private ObservableList<User> usersObservable = FXCollections.observableArrayList();

    public AdministratorServiceImpl(Logger logger){
        super(logger);
    }

    private void updateBooksObservable(){
        try{
            booksObservable.removeAll();
            booksObservable.addAll(findAllBooks());
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void updateUsersObservable(){
        try{
            usersObservable.removeAll();
            usersObservable.addAll(findAllUsers());
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public User createEmployee(UserRole role, String username, String password) {
        User user = userRepository.create(role, username, password);
        updateUsersObservable();
        return user;
    }

    @Override
    public User findEmployeeById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User updateEmployee(Long id, UserRole role, String username, String password) {
        User user = new UserBuilder()
                .withId(id)
                .withUserRole(role)
                .withUsername(username)
                .withPassword(password)
                .build();
        user = userRepository.update(user);
        updateUsersObservable();
        return user;
    }

    @Override
    public Boolean deleteEmployee(Long id) {
        Boolean result = userRepository.delete(id);
        updateUsersObservable();
        return result;
    }

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Book createBook(String genre, String title, String author, Long quantity, Double price) {
        Book book = bookRepository.create(genre, title, author, quantity, price);
        updateBooksObservable();
        return book;
    }

    @Override
    public Book findBookById(Long id) {
        return bookRepository.findById(id);
    }

    @Override
    public Book updateBook(Long id, String genre, String title, String author, Long quantity, Double price) {
        Book book = new BookBuilder()
                .withId(id)
                .withAuthor(author)
                .withGenre(genre)
                .withPrice(price)
                .withQuantity(quantity)
                .withTitle(title)
                .build();
        Book update = bookRepository.update(book);
        updateBooksObservable();
        return update;
    }

    @Override
    public Boolean deleteBook(Long id) {
        boolean delete = bookRepository.delete(id);
        updateBooksObservable();
        return delete;
    }

    @Override
    public List<Book> findAllBooks() {
        return bookRepository.findAll();
    }


    @Override
    public Boolean writeReport(String reportType) {
        ReportFactory reportFactory = new PDFReport();

        if(reportType.equals("PDF")){
            reportFactory = new PDFReport();
        } else if(reportType.equals("CSV")){
            reportFactory = new CSVReport();
        }

        return reportFactory.writeReport(getOutOfStockBooks());
    }

    @Override
    public List<Book> getOutOfStockBooks() {
        List<Book> allBooks = ClassInstances.getAdminUserService().findAllBooks();
        return allBooks
                .stream()
                .filter(b -> b.getQuantity() < 50)
                .collect(Collectors.toList());
    }

    public ObservableList<Book> getBooksObservable() {
        updateBooksObservable();
        return booksObservable;
    }

    public ObservableList<User> getUsersObservable() {
        updateUsersObservable();
        return usersObservable;
    }
}
