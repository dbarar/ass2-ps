package service.impl;

import model.Book;
import service.EmployeeService;
import utils.Notification;

import java.util.List;
import java.util.logging.Logger;

public class EmployeeServiceImpl extends UserServiceImpl implements EmployeeService {

    public EmployeeServiceImpl(Logger logger) {
        super(logger);
    }

    @Override
    public List<Book> searchBooksByGenre(String genre) {
        return bookRepository.findByGenre(genre);
    }

    @Override
    public List<Book> searchBooksByTitle(String title) {
        return bookRepository.findByTitle(title);
    }

    @Override
    public List<Book> searchBookByAuthor(String author) {
        return bookRepository.findByAuthor(author);
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Book sellBook(Long id) {
        Book book = bookRepository.findById(id);
        Long quantity = book.getQuantity();
        if (validation(quantity).hasErrors())
            throw new IllegalArgumentException(validation(quantity).errorMessage());

        book.setQuantity(quantity - 1);
        bookRepository.update(book);
        return book;
    }

    private Notification validation(Long quantity) {
        Notification note = new Notification();

        if (quantity == null) {
            note.addError("quantity cannot be null");
        }
        else if (quantity < 1){
            note.addError("quantity must be positive");
        }
        return note;
    }

}
