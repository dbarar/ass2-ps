package service;

import model.User;
import model.enumeration.UserRole;

public interface UserService {

    User login(String userName, String password);

    void logout();

}
