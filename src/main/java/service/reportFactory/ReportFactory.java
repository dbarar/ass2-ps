package service.reportFactory;

import model.Book;

import java.util.List;

public interface ReportFactory {

    Boolean writeReport(List<Book> list);
}
