package service.reportFactory;

import model.Book;
import utils.CSVUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVReport implements ReportFactory {

    private String csvFile = "developer.csv";
    private FileWriter writer;

    {
        try {
            writer = new FileWriter(csvFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean writeReport(List<Book> books) {
        //for header
        try {
            CSVUtils.writeLine(writer, Arrays.asList("Genre", "Author", "Title", "Quantity", "Price"));
            for (Book b : books) {
                List<String> list = new ArrayList<>();
                list.add(b.getGenre());
                list.add(b.getAuthor());
                list.add(b.getTitle());
                list.add(String.valueOf(b.getQuantity()));
                list.add(String.valueOf(b.getPrice()));

                CSVUtils.writeLine(writer, list);
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}
