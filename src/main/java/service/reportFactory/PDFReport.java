package service.reportFactory;
import java.io.FileNotFoundException;
import java.util.List;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;

import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Table;
import model.Book;

public class PDFReport implements ReportFactory {
    @Override
    public Boolean writeReport(List<Book> books){
        String file = "addingTableToPDF.pdf";

        //Creating a PdfDocument object
        PdfDocument pdfDoc = null;
        try {
            pdfDoc = new PdfDocument(new PdfWriter(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //Creating a Document object
        Document doc = new Document(pdfDoc);

        //Creating a table
        Table table = new Table(5);

        //Adding cells to the table
        table.addCell(new Cell().add("Genre"));
        table.addCell(new Cell().add("Title"));
        table.addCell(new Cell().add("Author"));
        table.addCell(new Cell().add("Quantity"));
        table.addCell(new Cell().add("Price"));

        for (Book book: books){
            table.addCell(new Cell().add(book.getGenre()));
            table.addCell(new Cell().add(book.getAuthor()));
            table.addCell(new Cell().add(book.getTitle()));
            table.addCell(new Cell().add(book.getQuantity().toString()));
            table.addCell(new Cell().add(book.getPrice().toString()));
        }

        //Adding Table to document
        doc.add(table);

        //Closing the document
        doc.close();
        return true;
    }
}
