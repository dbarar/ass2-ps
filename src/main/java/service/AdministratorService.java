package service;

import javafx.collections.ObservableList;
import model.Book;
import model.User;
import model.enumeration.UserRole;

import java.sql.Date;
import java.util.List;

public interface AdministratorService {
    //CRUD on Employee
    User createEmployee(UserRole role, String username, String password);

    User findEmployeeById(Long id);

    User updateEmployee(Long id, UserRole role, String username, String password);

    Boolean deleteEmployee(Long id);

    List<User> findAllUsers();

    //CRUD on Books
    Book createBook(String genre, String title, String author, Long quantity, Double price);

    Book findBookById(Long id);

    Book updateBook(Long id, String genre, String title, String author, Long quantity, Double price);

    Boolean deleteBook(Long id);

    List<Book> findAllBooks();

    //factory for 2 types of reports!!!!!!!!!!!!!!
    Boolean writeReport(String reportType);

    List<Book> getOutOfStockBooks();

    ObservableList<Book> getBooksObservable();

    ObservableList<User> getUsersObservable();
}
