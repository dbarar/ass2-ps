package service;

import model.Book;

import java.util.List;

public interface EmployeeService {
//      -	Search books by genre, title, author, or any combination of multiple filters.
//      -	Sell books.

    List<Book> searchBooksByGenre(String genre);

    List<Book> searchBooksByTitle(String title);

    List<Book> searchBookByAuthor(String author);

    List<Book> getAllBooks();

    Book sellBook(Long id);
}
