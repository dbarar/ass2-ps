package model.builder;

import model.Book;

public class BookBuilder {
    private Long id;
    private String genre;
    private String title;
    private String author;
    private Long quantity;
    private Double price;

    public BookBuilder withGenre(String genre){
        this.genre = genre;
        return this;
    }

    public BookBuilder withTitle(String title){
        this.title = title;
        return this;
    }

    public BookBuilder withAuthor(String author){
        this.author = author;
        return this;
    }

    public BookBuilder withQuantity(Long quantity){
        this.quantity = quantity;
        return this;
    }

    public BookBuilder withPrice(Double price){
        this.price = price;
        return this;
    }

    public BookBuilder withId(Long id){
        this.id = id;
        return this;
    }

    public Book build(){
        Book book = new Book();

        book.setId(id);
        book.setAuthor(author);
        book.setGenre(genre);
        book.setQuantity(quantity);
        book.setTitle(title);
        book.setPrice(price);

        return book;
    }
}
