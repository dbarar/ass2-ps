package model.builder;

import model.User;
import model.enumeration.UserRole;

public class UserBuilder {
    private Long id;
    private UserRole userRole;
    private String username;
    private String password;

    public UserBuilder withId(Long id){
        this.id = id;
        return this;
    }
    public UserBuilder withUsername(String id){
        this.username = id;
        return this;
    }
    public UserBuilder withPassword(String id){
        this.password = id;
        return this;
    }
    public UserBuilder withUserRole (UserRole userRole) {
        this.userRole = userRole;
        return this;
    }
    public User build(){
        User user = new User();

        user.setId(id);
        user.setUsername(username);
        user.setPassword(password);
        user.setUserRole(userRole);

        return user;
    }
}
