package model.enumeration;

public enum UserRole {
    USER,
    ADMIN
}