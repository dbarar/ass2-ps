package model;


import java.util.Observable;

public class Book extends Observable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String genre;
    private String title;
    private String author;
    private Long quantity;
    private Double price;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override public String toString(){
        return "Book: " +
                " id: " + id +
                " genre: " + genre +
                " title: " + title +
                " author: " + author +
                " quantity: " + quantity +
                " price: " + price;
    }
}
